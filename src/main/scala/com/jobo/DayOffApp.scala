package com.jobo

import java.time._
import java.time.format.DateTimeFormatter

import scala.util.Success

object DayOffCalculator {
  val publicHolidayDateArray = List("01-01", "01-06", "05-01", "05-03", "08-15", "11-01", "11-11", "12-25", "12-26")

  val currentYear = LocalDate.now().getYear

  val publicHol = publicHolidayDateArray.map(currentYear + "-" + _).map(parDate)
  val easter = getEasterSundayDate(currentYear)
  val easterSec = easter.plusDays(1)
  val corpusChristi = easter.plusDays(60)
  val pentecost = easter.plusDays(49)

  val s = publicHol ::: easter :: easterSec :: corpusChristi :: pentecost :: Nil

  def isDayOff(today: LocalDate = LocalDate.now()): Boolean =
    if (isWeekend(today)) true
    else s.exists(_.isEqual(today))

  private def parDate(date: String): LocalDate =
    LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"))

  private def getEasterSundayDate(year: Int) = {
    val a = year % 19
    val b = year / 100
    val c = year % 100
    val d = b / 4
    val e = b % 4
    val g = (8 * b + 13) / 25
    val h = (19 * a + b - d - g + 15) % 30
    val j = c / 4
    val k = c % 4
    val m = (a + 11 * h) / 319
    val r = (2 * e + 2 * j - k - h + m + 32) % 7
    val n: Int = (h - m + r + 90) / 25
    val p: Int = (h - m + r + n + 19) % 32

    LocalDate.of(currentYear, n, p)
  }

  private def isWeekend(date: LocalDate = LocalDate.now()) =
    Success(date.getDayOfWeek.getValue).filter(i => i == 0 || i == 6).map(_ => true).getOrElse(false)
}

object DayOffApp extends App {
  import DayOffCalculator._

  println(isDayOff())
}
